#!/usr/bin/env python3
from __future__ import annotations
import collections
import functools
import pathlib
import sys
import os
import textwrap
import typing


class File(typing.NamedTuple):
    path: pathlib.Path

    @classmethod
    @functools.lru_cache(maxsize=None)
    def from_path(cls, path: pathlib.Path) -> File:
        try:
            res = pathlib.Path(os.readlink(path))
        except OSError as e:
            # We can't read this link for some reason,
            # warn about it and continue.
            print(e, file=sys.stderr)
            res = path

        return cls(res)

    def is_shared(self) -> bool:
        """
        Can processes communicate through this file?
        """
        return pathlib.Path("/dev") not in self.path.parents

    def ident(self) -> str:
        return f"file{hash(self.path)}".replace("-", "_")

    def to_graphviz(self) -> str:
        return f'{self.ident()} [shape=note, label="{self.path}"];\n'


class Process(typing.NamedTuple):
    path: pathlib.Path
    cmdline: typing.List[str]
    descriptors: typing.Mapping[int, File]

    @classmethod
    @functools.lru_cache(maxsize=None)
    def from_path(cls, path: pathlib.Path) -> Process:
        raw_cmdline = path.joinpath("cmdline").read_text()
        cmdline = raw_cmdline.strip("\0").split("\0")

        descriptors = {}
        for each in path.glob("fd/*"):
            try:
                fd = int(each.name)
                file = File.from_path(each)
                descriptors[fd] = file
            except FileNotFoundError:
                continue

        return cls(path, cmdline, descriptors)

    def ident(self) -> str:
        return f"pid{self.path.name}"

    def to_graphviz(self) -> str:
        label = "\n".join(
            [
                textwrap.shorten(
                    " ".join(self.cmdline).replace("\\", "\\\\"),
                    40,
                ),
                "pid: " + self.path.name,
            ]
        )
        return f'{self.ident()} [label="{label}"];\n'


class Graph(typing.NamedTuple):
    by_pid: typing.Mapping[int, Process]
    by_file: typing.Mapping[File, typing.Set[int]]

    @classmethod
    def from_proc_tree(cls, path: pathlib.Path) -> Graph:
        by_pid = {}
        by_file = collections.defaultdict(set)

        for each in path.glob("*"):
            if not each.name.isnumeric():
                continue
            pid = int(each.name)
            proc = Process.from_path(each)

            by_pid[pid] = proc
            for file in proc.descriptors.values():
                by_file[file].add(pid)

        return cls(by_pid, by_file)

    def select_pipeline(self, pid: int) -> typing.List[Process]:
        seen_pids = set()
        check_now = {pid}
        check_next = set()

        while check_now:
            for pid in check_now:
                if pid not in self.by_pid:
                    continue
                proc = self.by_pid[pid]
                for file in proc.descriptors.values():
                    if file.is_shared():
                        check_next |= self.by_file.get(file, set())

            seen_pids |= check_now
            check_now = check_next - seen_pids
            check_next = set()

        return [self.by_pid[pid] for pid in seen_pids]


def pipeline_to_graphviz(
    pipeline: typing.List[Process],
    sink: typing.TextIO,
) -> None:
    sink.write("digraph {\n")
    for proc in pipeline:
        sink.write(proc.to_graphviz())

        for fd, handle in proc.descriptors.items():
            sink.write(handle.to_graphviz())
            if fd == 0:
                sink.write(f"{handle.ident()} -> {proc.ident()}\n")
                sink.write('  [headlabel="in"];\n')
            elif fd == 1:
                sink.write(f"{proc.ident()} -> {handle.ident()}\n")
                sink.write('  [taillabel="out"];\n')
            elif fd == 2:
                sink.write(f"{proc.ident()} -> {handle.ident()}\n")
                sink.write('  [taillabel="err"];\n')
            else:
                sink.write(f"{proc.ident()} -> {handle.ident()};\n")

    sink.write("}\n")


def main(args: typing.List[str]) -> int:
    graph = Graph.from_proc_tree(pathlib.Path("/proc"))

    if len(args) == 1:
        pid = os.getpid()
        dest = sys.stdout
        next_prog = None

    elif len(args) == 2:
        pid = int(args[1])
        dest = sys.stdout
        next_prog = None

    else:
        pid = os.getpid()
        dest = open(args[1], "w")
        next_prog = args[2:]

    pipeline = graph.select_pipeline(pid)
    pipeline_to_graphviz(pipeline, dest)

    if next_prog:
        dest.close()
        os.execvp(next_prog[0], next_prog)

    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))
