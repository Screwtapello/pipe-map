pipe-map
========

A tool to map out the components of a pipeline.
Given the PID of a running process,
it explores `/proc` for the file-handles that process has open,
other processes that have those files open,
the other files *those* processes have open,
and so forth
until all the processes participating in the pipeline are discovered.

The resulting network of processes and files
is output in the graph description format used by [GraphViz],
so you can render a diagram of how all the parts communicate.

[GraphViz]: https://www.graphviz.org/

Example 1: mapping a long-running pipeline
----------------------------------------

I launched the following pipeline in my shell:

    $ (cat | tr a-z A-Z | sort) 2>/dev/null

In another terminal,
I ran:

    $ pgrep -f sort
    588887

...to find the PID of the `sort` process,
then I generated the map of the pipeline
that process was part of:

    $ ./pipe-map.py 588887 > ./example-1.dot

You can look at [`example-1.dot`](./example-1.dot)
to see the raw output.
Then I used the GraphViz `dot` tool
to render it into a diagram:

    $ dot -Tpng ./example-1.dot > ./example-1.png

The resulting diagram looks like this:

![A diagram showing a multi-process pipeline](./example-1.png)

As you can see,
all the processes in the pipeline
have their "err" output routed to `/dev/null`,
while the regular output goes through a pipe
to the input of the next process.
The initial input and resulting output
are linked to `/dev/pts/3`,
the terminal where I ran the command.

Example 2: mapping the pipeline pipe-map is part of
---------------------------------------------------

Sometimes you want to map a pipeline that executes quickly,
perhaps part of a larger system.
pipe-map can map the pipeline it's part of,
and then execute another program.
If pipe-map is the first command,
that means the pipeline won't begin to flow
until the mapping is complete,
guaranteeing a complete and accurate map.

I launched the following command in my shell:

    $ (./pipe-map.py example-2.dot cat /etc/passwd | tr a-z A-Z | sort) 2>/dev/null

Compared to the previous example:

  - the first command in the pipeline is prefixed with `./pipe-map.py`
    and the filename where the resulting map should be stored
  - the `cat` command is given a file to read,
    so it won't hang around forever waiting for input

Once again,
you can look at [`example-2.dot`](./example-2.dot)
to see the raw output.
Then I used the GraphViz `dot` tool
to render it into a diagram:

    $ dot -Tpng ./example-2.dot > ./example-2.png

The resulting diagram looks like this:

![Another diagram showing a multi-process pipeline](./example-2.png)

This looks very similar to the previous example
(as it should!)
except that the first process in the pipeline is the `pipe-map.py` script,
before it executes `cat`.
While this is a slightly less accurate representation of the resulting pipeline,
it's a lot easier to create than trying to find the right PID
before the pipeline exits.

Requirements
------------

This tool requires:

  - Python 3
  - A Linux host with the `/proc` filesystem mounted.

TODO
----

  - Instead of hard-codng file-descriptor directions
    (0 is input, assume everything else is output)
    we should read the `open(2)` flags in `/proc/PID/fdinfo/FD`
    to see which descriptors are writable.
